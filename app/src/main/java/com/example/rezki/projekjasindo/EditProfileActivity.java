package com.example.rezki.projekjasindo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class EditProfileActivity extends AppCompatActivity {

    EditText etEditName, etEditEmail, etEditPhone, etEditAddress, etEditPassword, etEditConfirmPass;
    String stEditName, stEditEmail, stEditPhone, stEditAddress, stEditPassword, stEditConfirmPass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editprofile);

        etEditName = findViewById(R.id.etEditName);
        etEditEmail = findViewById(R.id.etEditEmail);
        etEditPhone = findViewById(R.id.etEditPhone);
        etEditAddress = findViewById(R.id.etEditAddress);
        etEditPassword = findViewById(R.id.etEditPassword);
        etEditConfirmPass = findViewById(R.id.etEditConfirmPass);

        SharedPreferences pref = getApplicationContext().getSharedPreferences("INI_PREF", MODE_PRIVATE);
        SharedPreferences.Editor editorProfile = pref.edit();
        stEditName = pref.getString("stName", "data tidak ada");
        stEditEmail = pref.getString("stEmail", "data tidak ada");
        stEditPhone = pref.getString("stPhone", "data tidak ada");
        stEditAddress = pref.getString("stAddress", "data tidak ada");
        stEditPassword = pref.getString("stPass", "data tidak ada");
        stEditConfirmPass = pref.getString("stConfirmPass", "data tidak ada");

        etEditName.setText(stEditName);
        etEditEmail.setText(stEditEmail);
        etEditPhone.setText(stEditPhone);
        etEditAddress.setText(stEditAddress);
        etEditPassword.setText(stEditPassword);
        etEditConfirmPass.setText(stEditConfirmPass);
    }

    public void klikSubmitEdit(View view){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("INI_PREF", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        stEditName = etEditName.getText().toString().trim();
        stEditEmail = etEditEmail.getText().toString().trim();
        stEditPhone = etEditPhone.getText().toString().trim();
        stEditAddress = etEditAddress.getText().toString().trim();
        stEditPassword = etEditPassword.getText().toString().trim();
        stEditConfirmPass = etEditConfirmPass.getText().toString().trim();

        editor.putString("stName", stEditName);
        editor.putString("stEmail", stEditEmail);
        editor.putString("stPhone", stEditPhone);
        editor.putString("stAddress", stEditAddress);
        editor.putString("stPass", stEditPassword);
        editor.putString("stConfirmPass", stEditConfirmPass);
        editor.apply();

        Intent i = new Intent(EditProfileActivity.this, ProfileActivity.class);
        startActivity(i);
        Toast.makeText(this, "Berhasil Ubah Profile!", Toast.LENGTH_SHORT).show();
        finish();
    }
}
