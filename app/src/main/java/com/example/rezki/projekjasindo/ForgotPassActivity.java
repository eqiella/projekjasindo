package com.example.rezki.projekjasindo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.rezki.projekjasindo.util.GMailSender;

import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class ForgotPassActivity extends AppCompatActivity {

    Button btn_sendFP;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgotpass);

        btn_sendFP = findViewById(R.id.btn_sendFP);
        btn_sendFP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {

                    GMailSender sender = new GMailSender(

                            "rezkinadia@gmail.com",

                            "Allahuakbar");


                    sender.sendMail("Test1", "Percobaan",

                            "rezkinadia@gmail.com",

                            "ekikaktus@gmail.com");

                    System.out.println("nyampe ");

//                    Intent i = new Intent(ForgotPassActivity.this, LoginActivity.class);
//                    startActivity(i);

                } catch (Exception e) {

                    Toast.makeText(getApplicationContext(),"Error", Toast.LENGTH_LONG).show();

                }


            }
        });
    }

    public void fp(){
        final String username = "rezki.nadia@indocyber.com";
        final String password = "indocyber";

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "465");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("rezki.nadia@indocyber.com"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse("rezkinadia@gmail.com"));
            message.setSubject("Testing Subject");
            message.setText("Dear Mail Crawler,"
                    + "\n\n No spam to my email, please!");

            MimeBodyPart messageBodyPart = new MimeBodyPart();

            Multipart multipart = new MimeMultipart();

            messageBodyPart = new MimeBodyPart();
            String file = "path of file to be attached";
            String fileName = "attachmentName";
            DataSource source = new FileDataSource(file);
            messageBodyPart.setDataHandler(new DataHandler(source));
            messageBodyPart.setFileName(fileName);
            multipart.addBodyPart(messageBodyPart);

            message.setContent(multipart);

            Transport.send(message);

            System.out.println("Done");

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(ForgotPassActivity.this, LoginActivity.class);
        startActivity(i);
    }
}
