package com.example.rezki.projekjasindo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    EditText etEmail;
    EditText etPassword;
    Button btnLogin;
    Button btnRegis;
    String dataEmail;
    String dataPass;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        etEmail = findViewById(R.id.loginEmail);
        etPassword = findViewById(R.id.loginPassword);
        btnLogin = findViewById(R.id.btn_login);
        btnRegis = findViewById(R.id.btn_regis);

        SharedPreferences prefLogin = getApplicationContext().getSharedPreferences("INI_PREF", MODE_PRIVATE);
        SharedPreferences.Editor editor = prefLogin.edit();

        dataEmail = prefLogin.getString("stEmail", "tidak ada data!");
        dataPass = prefLogin.getString("stPass", "tidak ada data!");
    }

    public void klikLogin(View view){
        String isianEmail = etEmail.getText().toString();
        String isianPassword = etPassword.getText().toString();
        System.out.println("INI ISI email: "+isianEmail);
        System.out.println("INI intent email: "+dataEmail);
        if (isianEmail.equals(dataEmail) && isianPassword.equals(dataPass)){
            System.out.println("email: "+dataEmail);
            Intent i = new Intent(LoginActivity.this, HomeActivity.class);
            startActivity(i);
            Toast.makeText(this, "BERHASIL LOGIN!", Toast.LENGTH_SHORT).show();
            finish();
        }else if (dataEmail==null && dataPass==null){
            Toast.makeText(this, "Silahkan lakukan registrasi dulu!", Toast.LENGTH_SHORT).show();
        }
        else Toast.makeText(this, "Email atau Password yang Anda masukkan salah!",
                Toast.LENGTH_SHORT).show();
    }

    public void klikForgotPass(View view){
        Intent i = new Intent(LoginActivity.this, ForgotPassActivity.class);
        startActivity(i);
        finish();
    }

    public void klikRegis(View view){
        Intent i = new Intent(LoginActivity.this, RegistrationActivity.class);
        startActivity(i);
    }
}
