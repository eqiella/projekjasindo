package com.example.rezki.projekjasindo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class ProfileActivity extends AppCompatActivity {

    TextView tblName, tblEmail, tblPhone, tblAddress, tblPassword, tblConfirmPass;
    String stEditName, stEditEmail, stEditPhone, stEditAddress, stEditPassword, stEditConfirmPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        tblName = findViewById(R.id.tblName);
        tblEmail = findViewById(R.id.tblEmail);
        tblPhone = findViewById(R.id.tblPhone);
        tblAddress = findViewById(R.id.tblAddress);
        tblPassword = findViewById(R.id.tblPassword);
        tblConfirmPass = findViewById(R.id.tblConfirmPass);

        SharedPreferences pref = getApplicationContext().getSharedPreferences("INI_PREF", MODE_PRIVATE);
        SharedPreferences.Editor editorProfile = pref.edit();
        stEditName = pref.getString("stName", "data tidak ada");
        stEditEmail = pref.getString("stEmail", "data tidak ada");
        stEditPhone = pref.getString("stPhone", "data tidak ada");
        stEditAddress = pref.getString("stAddress", "data tidak ada");
        stEditPassword = pref.getString("stPass", "data tidak ada");
        stEditConfirmPass = pref.getString("stConfirmPass", "data tidak ada");

        tblName.setText(stEditName);
        tblEmail.setText(stEditEmail);
        tblPhone.setText(stEditPhone);
        tblAddress.setText(stEditAddress);
        tblPassword.setText(stEditPassword);
        tblConfirmPass.setText(stEditConfirmPass);
    }

    public void klikSettingProfile(View view){
        Intent i = new Intent(ProfileActivity.this, EditProfileActivity.class);
        Toast.makeText(this, "Silakan Edit Profile yag diperlukan", Toast.LENGTH_SHORT).show();
        startActivity(i);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
