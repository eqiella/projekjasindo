package com.example.rezki.projekjasindo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Array;

public class RegistrationActivity extends AppCompatActivity {

    Button btnRegis;
    EditText etName, etEmail, etPhone, etAddress, etPass, etConfirmPass;
    String stName, stEmail, stPhone, stAddress, stPass, stConfirmPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrasi);
        btnRegis = findViewById(R.id.btn_registrasi);
        etName = findViewById(R.id.etRegisName);
        etEmail = findViewById(R.id.etRegisEmail);
        etPhone = findViewById(R.id.etRegisPhone);
        etAddress = findViewById(R.id.etRegisAddress);
        etPass = findViewById(R.id.etRegisPassword);
        etConfirmPass = findViewById(R.id.etRegisConfirmPassword);
    }

    public void klikRegistrasi(View view){
        stName = etName.getText().toString().trim();
        stEmail = etEmail.getText().toString().trim();
        stPhone = etPhone.getText().toString().trim();
        stAddress = etAddress.getText().toString().trim();
        stPass = etPass.getText().toString().trim();
        stConfirmPass = etConfirmPass.getText().toString().trim();

        SharedPreferences pref = getApplicationContext().getSharedPreferences("INI_PREF", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        editor.putString("stName", stName);
        editor.putString("stEmail", stEmail);
        editor.putString("stPhone", stPhone);
        editor.putString("stAddress", stAddress);
        editor.putString("stPass", stPass);
        editor.putString("stConfirmPass", stConfirmPass);

        editor.apply();

        Intent iLogin = new Intent(RegistrationActivity.this, LoginActivity.class);
        startActivity(iLogin);
        finish();
        Toast.makeText(this, "Berhasil Registrasi!", Toast.LENGTH_SHORT).show();
    }
}
