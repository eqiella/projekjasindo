package com.example.rezki.projekjasindo;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.example.rezki.projekjasindo.util.HttpHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class ScanActivity extends AppCompatActivity {

    private ListView lv;
    private static String url;
    private ProgressDialog pDialog;
    private String TAG = ScanActivity.class.getSimpleName();
    ArrayList<HashMap<String, String>> docsList;
    AlertDialog.Builder dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);
        lv = findViewById(R.id.list);
        lv.setDivider(null);

        String link = null;
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            link = bundle.getString("valueLink");
            url = link;
            System.out.println("ini url: "+url);
        }
        docsList = new ArrayList<>();
        if (url.contains("http")){
        new getDocs().execute();
        }
        else {
            DialogForm();
        }
    }

    private void DialogForm(){
        dialog = new AlertDialog.Builder(ScanActivity.this);
        dialog.setCancelable(true);
        dialog.setTitle("Ini isi: "+url);

        dialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.dismiss();
                Intent intent = new Intent(ScanActivity.this, HomeActivity.class);
                startActivity(intent);
            }
        });
        dialog.show();
    }

    private class getDocs extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(ScanActivity.this);
            pDialog.setMessage("Silahkan tunggu...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpHandler sh = new HttpHandler();
            String jsonstr = sh.makeServiceCall(url);
            Log.e(TAG, "Request dari url: " + jsonstr);

            if (jsonstr != null) {
                try {
                    JSONObject object = new JSONObject(jsonstr);
                    JSONObject jobRes = object.getJSONObject("response");
                    String numFound = jobRes.getString("numFound");
                    System.out.println("INI numFound: "+numFound);
                    JSONArray arrDocs = jobRes.getJSONArray("docs");

                    for (int i=0; i<arrDocs.length() ;i++){
                        JSONObject idObj = arrDocs.getJSONObject(i);
                        String id = idObj.getString("id");
                        System.out.println("ini id: "+arrDocs);
                        HashMap<String, String > recip = new HashMap<>();
                        recip.put("id",id);
                        docsList.add(recip);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            ListAdapter adapter =new SimpleAdapter(
                    ScanActivity.this, docsList,
                    R.layout.content_listview,new String[]{"id"},
                    new int[]{R.id.tvId}
            );
            lv.setAdapter(adapter);
            pDialog.cancel();
            System.out.println("selesai");
        }
    }
}
